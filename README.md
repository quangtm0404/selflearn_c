## Private Repository to learn C++
**Progress: 11% (13 / 123)**
## References
Resources: Learn from `roadmap.sh`
<a href="https://roadmap.sh/cpp" style="text-color:black;">RoadMap</a>

### Note
- Default Value of a pointers is 8 bytes (64 bit) but it depends on addresses space of the system (32 bit will allocate 32 bit - 4 bytes) - (64 will allocate 64-bytes)
- char* and int* difference that the datatype of the variable  they are pointed to