## Functions in C++
**I. Defining Functions**
```c++
return_type function_name(parameters) {
    body
}
```

Note: If functions is below `main.cpp` - we must declare at top (`function prototype`)
**II. Function Prototypes**
```c++
#include <iostream>
using namespace std;

// Function prototype
int multiplyNumbers(int x, int y);

int main() {
    int num1 = 3, num2 = 7;
    int result = multiplyNumbers(num1, num2); // Calling the function
    cout << "The product is: " << result << endl;
    return 0;
}

// Function definition
int multiplyNumbers(int x, int y) {
    int product = x * y;
    return product;
}
```

***
### Lambda Functions
- Lambda Functions (dùng cho hàm vô danh)

**I.Syntax**
```c++
[capture-list](parameters) -> return type {
    
};
```
- `capture-list` : là list các biến mà lambda ex có thể access

**II. Usage**

**1. void method, no params**
```c++
auto printHello = []() {
    std::cout << "Hello World!" << std::endl;
};
```

**2. With parameters**
```c++
auto add = [](int a, int b) -> int {
    return a + b;
};

```
**3. With Capture by value**
```c++
int multiplier = 3;
auto times = [multiplier](int a) {
    return a * multipler;
};
int result = times(5);


```
**4. Capture-by-reference**
```c++
int expiresInDays = 45;
auto updateDays = [&expiresInDays](int newDays) {
    expiresInDays = newDays;
};
updateDays(30);
```