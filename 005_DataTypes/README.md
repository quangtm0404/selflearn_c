#Data Types in C++
#Sections
[Fundamentals Data Types](#ifundamental-data-types)
[Derived Data Types](#ii-derived-data-types)
[User-Defined Types](#iii-user-defined-data-types)
[StaticTyping](#static-typing)
[Dynamic Typing](#dynamic-typing)
[RTTI](#rtti-run-time-type-identification)

### I.Fundamental Data Types
####1. Integer (int)
- default chiếm 4 bytes
`int num = 42;`
####2. Floating point Data types (float, double)
float (4 bytes), double (8 bytes)
####3. Characters 
`char a = 'A';` - 1 byte
####4. Boolean (bool)
True False value
`bool isTrue = true;` - 1 byte

### II. Derived Data Types
- Data types which derived from fundamental datatypes
####1. Arrays 
store mulitple values of a datatypes, access through index - take memory by which of its data types (example: int is 4 bytes for each elements)
`int numbers[5] = {1, 2, 3, 4, 5};` - int (4 bytes) - 5 elements - 20 bytes 
####2. Pointers
`Pointer` is a datatype to store address of variable
`int num = 42;`
`int* nump = &num;`

####3. References
`References` is an alternative way to get memory address of variable
`int num = 42;`
`int& numeRef = num;`
**Diferrences of Pointers and References**
<table>
<thead>
<tr>
    <th></th>
    <th>References</th>
    <th>Pointers</th>
    
</tr>
</thead>
<tbody> 
    <tr>
    <td>Reassignment</td>
    <td> 
    The variable cannot be reassigned in Reference.
    </td>
    <td> The variable can be reassigned</td>
    </tr>
    <tr> 
        <td>Memory Address</td>
        <td>Share the same address as the original var</td>
        <td>Pointers have their own memory</td>
    </tr>
    <tr>
        <td>Work</td>
        <td>Use to referring to another var</td>
        <td>Use to store address of a variable</td>
    </tr>
    <tr>
        <td>Null Value</td>
        <td>Does not nullable</td>
        <td>Nullable</td>
    </tr>
    <tr>
    <td>Arguments</td>
    <td>This variable is referenced by the method pass by value</td>
    <td>The pointer does it work by the method known as pass by reference.
    </td>
    </tr>
</tbody>
</table>

### III. User-Defined Data Types
This is a data type which is defined by programmer
####1. Structures (struct)
Structures là class không có methods
``````c++
struct Person {
    int age;
    string name;
    float height;
};

Person person = {20, "Jonh Doe", 1.7};
``````

####2. Classes (class)
Có member functions và `access specifiers`
``````c++
class Person {
    public : 
    string name;
    int age;
    void printInfo() {
        cout << "Name: " << name << ", Age: " << age << endl;
    };
};

Person p1;
p1.name = "Quang";
p1.age = 30;
``````

####3. Unions
Use to store different data types with same memory allocation
```c++
union Data {
    int num;
    char letter;
    float decimal;
}


Data myData;
myData.num = 42;
```

***
# Static Typing 
Là kiểu dữ liệu được declare trước quá trình biên dịch. Không thể thay đổi datatypes của biến đó
`int num = 42;` -- mình không thể gán float cho num

# Dynamic Typing
Dù C++ là ngôn ngữ kiểu statically-type-language, có nghĩa là data type được fix cứng trước khi biên dịch, nhưng nó cũng có hỗ trợ dynamic types

##1. Void Pointers 
`void* ` là generic pointers that can point to objects of any data types
void pointer có thể trỏ đến bất cứ kiểu dữ liệu nào
```c++
#include <iostream>

int main() {
    int x = 42;
    float y = 3.14f;
    std::string z = "Hello, world!";

    void* void_ptr;

    void_ptr = &x;
    std::cout << "int value: " << *(static_cast<int*>(void_ptr)) << std::endl;

    void_ptr = &y;
    std::cout << "float value: " << *(static_cast<float*>(void_ptr)) << std::endl;

    void_ptr = &z;
    std::cout << "string value: " << *(static_cast<std::string*>(void_ptr)) << std::endl;

    return 0;
}
```

##2. `std::any`
cast được về mọi type, một class generic hỗ trợ cho `C++ 17`
```C++
#include <iostream>
#include <any>

int main() {
    std::any any_value;

    any_value = 42;
    std::cout << "int value: " << std::any_cast<int>(any_value) << std::endl;

    any_value = 3.14;
    std::cout << "double value: " << std::any_cast<double>(any_value) << std::endl;

    any_value = std::string("Hello, world!");
    std::cout << "string value: " << std::any_cast<std::string>(any_value) << std::endl;

    return 0;
}
```

***
# RTTI (Run-Time Type Identification)
## Definition
- RTTI là chức năng của `C++` hỗ trợ ta thu thập type info của `obj` trong quá trình execution. Tiện lợi khi ta sử dụng dynamic typing
- Có hai mechanism chính cho `RTTI` trong C++
    `typeid` operator
    `dynamic_cast` operator

### `typeid` operator
`typeid` sẽ return về reference của một obj theo type `std::type_info`, nó chứa thông tin về type của obj đó
**Header file là `<typeinfo>`**

### `dynamic_cast`
Là phép cast type trong runtime, quăng bad_cast_exception nếu cast failed
