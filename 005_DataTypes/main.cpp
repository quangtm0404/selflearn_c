


// int main() {
// class Person {
//     public : 

//     int age;
//     void toString() {
//         cout <<"Age: " << age << endl;
//     };

// };

//     Person p1;
//     cout << "Please input age: ";
//     cin >> p1.age;
    
//     p1.toString();
//     return 0;
// }

// typeid demo
// #include <iostream>
// #include <typeinfo>
// using namespace std;
// class Base {virtual void dummy() { }};
// class Derived : public Base{ };
// int main() {
//     Base* base_ptr = new Derived;
//     cout << "Type: " << typeid(*base_ptr).name() << endl;
//     delete base_ptr;
//     return 0;
// }



// dynamic_cast demo
#include <iostream>
using namespace std;

class Base { virtual void dummy() {} };
class Derived1 : public Base { /* ... */ };
class Derived2 : public Base { /* ... */ };

int main() {
    Base* base_ptr = new Derived1;
    Derived1* derived1_ptr = dynamic_cast<Derived1*>(base_ptr);
    if(derived1_ptr) {
        cout << "Cast Derived1 successfully!" << endl;
    }
    Derived2* derived2_ptr = dynamic_cast<Derived2*>(base_ptr);
    if(derived2_ptr != NULL) {
        cout << "Cast 2 successfully!";
    } else {
        cout << "Cast 2 failed!";
    }
    return 1;
}