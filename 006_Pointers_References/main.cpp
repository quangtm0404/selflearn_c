#include <iostream>
 using namespace std;
// void swap(int& a, int &b) {
//     int temp = a;
//     a = b;
//     b = temp;
// }
// int main() {
//     int x = 5, y = 10;
//     swap(x, y);
//     cout << "x: " << x << ", y: " << y << endl;
//     return 0;
// }
// void swap(int* a, int* b);

// int main() {
//     int x = 5;
//     int y = 10;
//     swap(x , y);
//     cout << "x: " << x << ", y: " << y << endl;
//     return 0;
// }

// void swap(int* a, int* b) {
//     int* temp = a;
//     a = b;
//     b = temp;
// }

int main() {
    int num = 10;
    int* num_ptr = &num;
    cout << *num_ptr << endl;
    return 1;
}