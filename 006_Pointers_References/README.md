# Pointers & References
## Section `#`
[Introduction And Definition](#definition)
[References](#iireferences)

# Definition
### **1. Pointers**
Pointers is a variable use to store memory address of a variable or functions, it is nullable, allows you to access or modify indirectly
**Syntax** `datatype* name_ptr;`

**Initialize ptr**
```c++
int num = 10;
int* num = &num;
```
**Accessing value of ptr**
`int value = *ptr;`
value now store value which the pointer `ptr` point into

### **2. References**
Reference is an alias for an existing variable (different name but the same memory location). Reference is not nullable, must be init when it is declared, 
**Syntax**
`datatype& referenceName = existing_variable;`

**Usage**
```c++
int num = 10;
int& num_ref = num;
num_ref = 12; // in this line, num = 12 because num_ref and num store the same memory location
```
***
# II.References
References can be considered as a `constant pointer`, it has to be initialized when declare, and it can not change the value (always point to the same object)

- Declaration and Initialization
```c++
int var = 10;
int& ref = var;
```

- Usage: 
Use as the original variable. But when you change the value of `ref`, the value of the variable is also changed, because is share the same memory location
```c++
var = 20;
cout << ref << endl;

ref = 30;
cout << var << endl; // this print 30
```

Or we can use it as a function parameters
```C++
void swap(int& a, int& b) {
    int temp = a;
    a = b;
    b = temp;
}

```



