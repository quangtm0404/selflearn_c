# Smart Pointers
Là pointer cấp phát bộ nhớ động, low level constructs, directly hold a memory address
## I. `new` operator
- operator dùng để cấp phát memory ở `heap` 
- the memory which we are allocating by `new` operator will not be cleared until we `deallocate` by using `delete`

```c++
int* ptr = new int; // Cấp phát bộ nhớ cho int trong heap
*ptr = 42; // Gán value cho vùng nhớ ta vừa allocate
```

## II. `delete` operator
- `delete` is an operator we are using to `deallocate` the memory that has been allocate via `new` operator
- If we are not allocate, it will lead to `memory leaks`
```c++
int* ptr = new int;
*ptr = 42;
delete ptr;
```

## III. `new[]` and `delete[]`
- Chỉ là cách allocate và deallocate for arrays of objects
- It is appeared because we can not allocate and deallocate each elements of an arrays, take times and it very dummy
```c++
int n = 10;
int* arr = new int[n];
for(int i = 0; i < n; i ++) {
    arr[i] = i;
}

delete[] arr;
```

### Summary
- `new` and `delete` help us can manually manage memory in C++, we can allocate (cấp phát vùng nhớ) and deallocate (xoá vùng nhớ)
- Make sure to deallocate memory, if not, it will lead to `memory leaks`

***
# Memory Leakage
- `Memory Leakage`(tràn vùng nhớ) là hiện tượng khi ta `allocate` memory mà quên giải phóng chúng (khi được lưu ở heap), làm cho program ngày càng nặng hơn và dẫn đến crash
- Trong C++, khi sử dụng raw pointers, ta luôn phải quan tâm đến vấn đề này, vì vùng nhớ của raw pointers được quản lý manually
```C++
void create_memory_leak() {
    int* ptr = new int[100];
}
```
- Để tránh trường hợp memory leak, ta nên deallocate trước khi pointer go out of scope. Hoặc ta có thể sử dụng một số techniques
1. sử dụng C++ smart pointers (`std::unique_ptr`, `std::shared_ptr`)
2. RAII 
3. Một số C++ standard lib có manage memory (`std::vector`, `std::string`)
```c++
#include<memory> 
void no_memory_leak() {
    std::shared_ptr<int> ptr = std::make_shared<int[]>(100);
} // If go out of scope, shared_ptr will automatically deallocated
```