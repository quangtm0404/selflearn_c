#include <iostream>
int getBinaryValue(int number);

int main() {
    int number = 11;
    int result = 11 >> 1;
    std::cout << getBinaryValue(result) << std::endl;
    
    return 1;
}

int getBinaryValue(int number) {
    int result = 0;
    int i = 1;
    int remainder;
    while(number != 0) {
        remainder = number % 2;
        number /= 2;
        result = result + remainder * i;
        i *= 10;
    }
    return result;
}