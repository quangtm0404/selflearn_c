## Basic Operators
***I. Arithmetic Operations***
`+, - , *, /, %`
`++x, x++, --x, x--` - incremental, decrememtal operators
***II. Relational Operators***
1. Equal to `==`
`a == b;`
Check equivalent, return boolean

2. `!=` - Check unequivalent, return boolean
3. `>`
4. `>=`
5. `<=`

***Logical Operators***
**1. AND `&&`**
return true if both is true
**2. OR `||`**
return true whether one of them is true
**3. NOT `!`**
return oppsite val of expression, var

***III. Bitwise Operators***
**1. `&` AND bitwise**
- phép AND dựa trên `binary value` của var 

**2. `|` OR bitwise**
- Phép OR dựa trên `binary value`
`0001 | 0100 --> 0101`

**3. `^` XOR**
**4. `~` NOT**
**5. `<<` Left shift**
**6. `>>` Right shift**

