# Session 001 _ First step of gigantic
## Introduction with `C++`
### **I. What is `C++`, Basic of `C++`**
**1. Including Libraries**
`#include<iostream>`
- Dùng để include các thư viện cần thiết


**2. Main functions**
entry point của `C++`, hàm main: 
```C++
int main() {

    return 0;
}
```


**3. Input/Output**
- Về cơ bản thì `<<` cho output và `>>` cho input
- `std::cin` và `std::cout` (Nhận input và trả input ra console)
```C++
#include<iostream>
int main() {
    int number;
    std::cout << "Enter Interger: ";
    std::cin >> number;
    std::cout << "Your number " << number << std::endl;
    return 0;
}
```

**4. Variables and Data Types**
C++ cũng có các kiểu data type như các nnlt khác 
- `int`
- `float`
- `char`
**5 Conditional Statements**
```c++
if(expression) {

} else if(bool expression){

} else {

}
```

`switch(var) {case case case}`
- include break statement

*Loop*
- `for(); while(); do{ }while();`

**6. Functions**
The totally concepts of
block of codes to reuse efficiently


