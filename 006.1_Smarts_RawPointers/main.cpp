// Unique pointer
// #include <iostream>
// #include <memory>
// using namespace std;
// int main() {
//     unique_ptr<int> p1 = make_unique<int>(10);
//     unique_ptr<int> p2 = move(p1);
//     // At this time p1 is null
//     if(p1) {
//         cout << "p1 own" << endl;
//     }else {
//         cout << "p2 own the 10 value" << endl; 
//     }
//     return 0;
// }


// shared_ptr
// #include <iostream>
// #include <memory>
// using namespace std;
// class MyClass {
//     public : 
//     MyClass() { cout << "Constructor is called" << endl; }
//     ~MyClass() {cout << "Destructor is called" << endl; }
// };

// int main() {
//     shared_ptr<MyClass> ptr1(new MyClass());
//     {
//         shared_ptr<MyClass> ptr2 = ptr1;
//         cout << "Inside Scope of ptr2" << endl;
//         // When leaving this scope, system will deallocate ptr2
//     }

//     cout << "outside of ptr2's scoped" << endl;
    
//     // When main func return _ system will deallocate ptr1, and the memory which we allocate to store the MyClass instance will be cleared
//     return 1;
// } 
// // Only destroyed when both ptr go out of scope



// Weak pointer
#include <iostream>
#include <memory>
using namespace std;
class MyClass {
public:
    void DoSomething() {
        cout << "Doing something...\n";
    }
};

int main() {
    weak_ptr<MyClass> weak;

    {
        shared_ptr<MyClass> shared = make_shared<MyClass>();
        // weak = shared;

        // if(auto sharedFromWeak = weak.lock()) {
        //     sharedFromWeak->DoSomething(); // Safely use the object
        //     cout << "Shared uses count: " << sharedFromWeak.use_count() << endl; // 2
        // }

    }

    // shared goes out of scope and the MyClass object is destroyed

    if(auto sharedFromWeak = weak.lock()) {
        // This block will not be executed because the object is destroyed
    }
    else {
        cout << "Object has been destroyed\n";
    }

    return 0;
}