# Smart Pointers
### Section list
[I. Unique Pointers](#unique-pointers-stdunique_ptr)
[II. Share Pointers](#2-share_ptr-_-share-pointer)
[III. Weak Pointer](#3-weak-pointer-weak_ptr)
***
## 1.Unique Pointers (`std::unique_ptr`)
- `std::unique_ptr` is a smart pointer provide by C++ standard library.
- template class use to manage single object or array
- work on concept `exclusive ownership` - chỉ có một `unique_ptr` được phép own obj trong thời điểm. `Owner` ship có thể transfer hoặc moved. nhưng không thể shared hoặc copied __ Concept này giúp loại bỏ issue của `dangling pointer` - giảm memory leaks. 
- Khi ra khỏi scope _ `unique_ptr` sẽ được tự động deallocate

## Creating
```c++
#include <iostream>
#include <memory>
using namespace std;
int main() {
    unique_ptr<int> p1(new int(5));
    unique_ptr<int> p2 = make_unique<int>(10);
    cout << * p1 << ", " << *p2;
    return 0;
}
```

## II. Transferring Ownership
```c++
#include <iostream>
#include <memory>
using namespace std;
int main() {
    unique_ptr<int> p1 = make_unique<int>(10);
    unique_ptr<int> p2 = move(p1);
    // At this time p1 is null
    if(p1) {
        cout << "p1 own" << endl;
    }else {
        cout << "p2 own the 10 value" << endl; 
    }
    return 0;
}
```

## III. Using `unique_ptr` with Custom Deleters
```c++
#include <iostream>
#include <memory>

struct MyDeleter {
    void operator()(int* ptr) {
        std::cout << "Custom Deleter: Deleting pointer" << std::endl;
        delete ptr;
    }
};

int main() {
    std::unique_ptr<int, MyDeleter> p1(new int(5), MyDeleter());
    return 0; // Custom Deleter will be called when p1 goes out of scope
}
```
**Remind that an `unique_ptr` is exclusive ownership, if we want to share the acessesible, we should use `shared_ptr`**

***
# 2. `share_ptr` _ Share Pointer
## I. Definition
- `shared_ptr` là smart pointer mà nó allow multiple ownership 
- memory của object ta trỏ tới sẽ được tự động `deallocated` chỉ khi `shared_ptr` cuối cùng trỏ đến nó bị destroyed (out of scope)
- Khi sử dụng `shared_ptr`, reference counter sẽ tự động ++ mỗi khi có pointer mới trỏ đến, và tự -- mỗi khi pointer đó out of scope, khi lượng reference == 0, system sẽ tự động clear vùng nhớ chứa dữ liệu
## II. Usage 
Demo at `main.cpp
```C++
#include <iostream>
#include <memory>
using namespace std;
class MyClass {
    public : 
    MyClass() { cout << "Constructor is called" << endl; }
    ~MyClass() {cout << "Destructor is called" << endl; }
};

int main() {
    shared_ptr<MyClass> ptr1(new MyClass());
    {
        shared_ptr<MyClass> ptr2 = ptr1;
        cout << "Inside Scope of ptr2" << endl;
        // When leaving this scope, system will deallocate ptr2
    }

    cout << "outside of ptr2's scoped" << endl;
    
    // When main func return _ system will deallocate ptr1, and the memory which we allocate to store the MyClass instance will be cleared
    return 1;
} 
// Only destroyed when both ptr go out of scope
```
***
# 3. Weak Pointer `weak_ptr`
## I. Definition
- `weak_ptr` cũng là một dạng smart pointer mà nó add thêm một level indirection và safety hơn với raw pointer
- Nó dùng để giảm tránh reference cycle khi hai objject shared pointer to each other, hoặc ta cần một nonowning reference tới object managed bởi một `shared_ptr`
- Một weak pointer sẽ không ++ ref count khi trỏ tới object, dùng để assure là object sẽ được clean khi `shared_ptr` cuối cùng out of scope, dù `weak_ptr` vẫn referencing

- Để sử dụng `weak_ptr`, ta cần phải convert nó sang `shared_ptr`  sử dụng hàm `lock()`, nó sẽ tries to create new `shared_ptr` 

## II. Usage
```c++
#include <iostream>
#include <memory>
using namespace std;
class MyClass {
public:
    void DoSomething() {
        cout << "Doing something...\n";
    }
};

int main() {
    weak_ptr<MyClass> weak;

    {
        shared_ptr<MyClass> shared = make_shared<MyClass>();
        weak = shared;

        if(auto sharedFromWeak = weak.lock()) {
            sharedFromWeak->DoSomething(); // Safely use the object
            cout << "Shared uses count: " << sharedFromWeak.use_count() << endl; // 2
        }
    }

    // shared goes out of scope and the MyClass object is destroyed

    if(auto sharedFromWeak = weak.lock()) {
        // This block will not be executed because the object is destroyed
    }
    else {
        cout << "Object has been destroyed\n";
    }

    return 0;
}
```

**Resources**
[CppReference](https://en.cppreference.com/w/cpp/memory/weak_ptr)