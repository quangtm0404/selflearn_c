# Memory Model in C++
Memory model define how the program store and accesses in data in computer memory. It contains different segments like `Stack`, `Heap`, `Data`, `Code` 
[Stack Memory](#i-stack-memory)
[Heap Memory](#ii-heap-memory)
[Data Segment](#iii-data-segment)
[Code Segment](#iv-code-segment)

## I. Stack Memory
- use to store automatic storage duration variables, like local var and function call data. `Stack` is managed by compiler, so it will allocate and deallocate automatically. 
- Stick with `LIFO` - var allocate last will deallocate first -- Stack in the nutshell
```c++
void function() {
    int x = 10; // store in stack
}
```
## II. Heap Memory
- Dynamic storage duration variables, like objects created by the `new` keyword
- We have to manually allocate and deallocate memory in Heap with `new` and `delete` 
in Section ***006.2_SmartPointers***
- `Heap`` has a larger pool, but accessess time lower than `Stack`
```c++
void function() {
    int* p = new int; // allocate in heap
    *p = 10; 
    delete p; // deallocate memory
}
```
### III. Data Segment
- Data segment has the `initialized segment` and `uninitialized segment`
- **`initialized segment`** use to store const, global var, static var which had init val
- **`uninitialized segment`**: opposite with `initialized segment`, the const, global var, static var which is not has init val yet

```C++
int global_var = 10; // allocate in init data segment
static int static_var = 10; 
const int constVar = 10;

int global_var_1; // allocate in uninit data segment
```
### IV. Code Segment
- We can call it as Text segment
- The memory to store exe code (native language) to execute by computer.
- It provide a **`readonly area memory`** to **prevent modification**

***
# Object Lifetime in C++
Dùng để định nghĩa về thời gian từ khi object được khởi tạo cho tới khi object bị huỷ khỏi vùng nhớ, có 4 loại 
1. Static Storage Duration
Những object với `static storage duration` tồn tại trong suốt quá trình chạy chương trình, hoặc đơn giản là những gì được lưu trong code segment - allocate ở beginning of program and deallocate at the end
```C++
int global_var;
class MyClass {
    static int static_var; // static storage duration
};
void myFunction() {
    static int local_var;
}
```
2. Thread Storage Duration
- Exist for the lifetime of the thread
- Start with thread and end with thread
`thread_local int my_var;`
3. Automatic Storage Duration
- Its a duration usually use for `stack` location (local var - nonstatic)
- created at the point of its definition and destroy when out of scope
```C++
void myFunction() {int var = 10;} // automatic storage duration
```
4. Dynamic Storage Duration
- Create at runtime, using memory allocation by `new`-`malloc` 
- Lifetime will be managed manually by programmer, destroy by `delete` - `free`
- Be careful once again - its gonna lead to **Memory Leaks** if we are not deallocate
````C++
int* ptr = new int;        // Dynamic storage duration
delete ptr;
``````